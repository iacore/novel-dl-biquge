"""
Usage:
curl https://www.biqugeuu.com/93638_93638383/508704807.html | python extract-chapter.py /dev/stdin
"""

import sys
from bs4 import BeautifulSoup
try:
	doc = BeautifulSoup(open(sys.argv[1], encoding="GB18030").read(), 'lxml')
except UnicodeDecodeError:
	doc = BeautifulSoup(open(sys.argv[1]).read(), 'lxml')
title = doc.select_one('h1').text.strip()

el_content = doc.select_one('#content')
nodes=el_content.contents

done = False
while not done:
	done = True
	for i,node in enumerate(nodes):
		if isinstance(node, str): continue
		if node.name != "script": continue
		if len(nodes) - i <= 6:
			done = False
			nodes = nodes[:i]
			break
		if i < 5:
			done = False
			nodes = nodes[i+1:]
			break

assert len(nodes) > 7

content = ""
for node in nodes:
	if isinstance(node, str):
		content += node
	elif node.name == "br":
		content += "\n"

sys.stdout.write(title)
sys.stdout.write('\n'*3)
sys.stdout.write(content)
sys.stdout.write('\n'*5)
