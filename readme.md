Experimental.

## Usage

```
wget2 -rk --no-parent https://www.biqugeu.net/93638_93638383/
python  list-chapters.py www.biqugeu.net/93638_93638383/index.html > www.biqugeu.net/chapters.txt
for a in (cat www.biqugeu.net/chapters.txt)
	set f www.biqugeu.net/93638_93638383/$a
	python extract-chapter.py $f
end > out.txt
```

## todo

simple
- sibling analysis to find actual TOC
- use readability to extract content
- manual postprocessing rules, to remove trash from content

hard
- use overlap analysis: check common content between pages, and remove them (probably garbage data)

