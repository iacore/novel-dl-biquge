"""
Usage:
curl https://www.biqugeuu.com/93638_93638383/ | python list-chapters.py /dev/stdin
"""

import sys
from bs4 import BeautifulSoup
doc = BeautifulSoup(open(sys.argv[1], encoding="gbk").read(), 'lxml')
# too slow:
links = doc.select('dt ~ dt ~ dd a')
assert len(links) > 0

for link in links:
	print(link['href'])
